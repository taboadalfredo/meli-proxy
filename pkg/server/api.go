package server

import (
	"encoding/json"
	"html"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

type api struct {
	router http.Handler
}

type Server interface {
	Router() http.Handler
}

func New() Server {
	a := &api{}
	r := mux.NewRouter()
	r.HandleFunc("/{.*}", a.fetchMeli)
	r.HandleFunc("/{.*}/{.*}", a.fetchMeli)
	a.router = r

	return a
}

func (a *api) Router() http.Handler {
	return a.router
}

func (a *api) fetchMeli(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	method := r.Method
	path := html.EscapeString(r.URL.Path)
	ip := GetIP(r)
	canRequest, err := CanRequest(ip, path)
	if canRequest {
		body, err := RetrieveURLData(method, path)
		if err != nil {
			json.NewEncoder(w).Encode(err)
		} else {
			b := []byte(body)
			var f interface{}
			json.Unmarshal(b, &f)
			json.NewEncoder(w).Encode(f)
		}
	} else {
		json.NewEncoder(w).Encode(err.Error())
	}

}

// GetIP parse ip from request
func GetIP(r *http.Request) string {
	forwarded := r.Header.Get("X-FORWARDED-FOR")
	if forwarded != "" {
		return forwarded
	}
	return r.RemoteAddr
}

//CanRequest validate on cache if the request don't exced the limits per server ip.
func CanRequest(ip string, path string) (bool, error) {
	//if cache.GetLlen(ip) > 50000 {
	//	return false, errors.New("Request limit by ip exceded")
	//}
	//if cache.getLlen(ip+path) > 50000 {
	//	return false, errors.New("Request limit by ip+url exceded")
	//}
	return true, nil
}

//RetrieveURLData curl to path using method returning the body of the response.
func RetrieveURLData(method string, path string) (string, error) {
	//extract base url to configuration file or parameterize it.
	url := "http://api.mercadolibre.com" + path
	req, err := http.NewRequest(method, url, nil)
	res, err := http.DefaultClient.Do(req)
	body, err := ioutil.ReadAll(res.Body)

	return string(body), err

}
