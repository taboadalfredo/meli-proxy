# MELI - Proxy
 
## General
 
La solución consiste en una aplicación desarrollada en Go, dockerizada que utiliza una memoria cache montada sobre Redis con un esquema maestro-esclavo.
 
Se evalúan dos lenguajes para el desarrollo de la aplicación: Ruby y GO. Al ser una aplicación netamente backend y teniendo en cuenta la cantidad de RPM que debe soportar, se opta por usar GO es más rápido que Ruby.
 
La memoria cache estará montada sobre Redis en un esquema maestro-esclavo para contemplar un posible escalado horizontal. Redis además cuenta con un esquema de colecciones que reduce los conflictos de concurrencia cuando se intentar ingresar valores para una misma clave.
 
Los requests serán atendidos por un load balance que distribuirá la carga sobre las distintas instancias de la aplicación. En caso de ser necesario se podrán agregar instancias de la aplicación para un eventual escalado del proxy. Las distintas instancias de la aplicación se comunicaran con un servidor redis para validar la cantidad de RPS que puede hacer un servidor(identificado por su ip). Si la aplicación escala, se pueden agregar nuevos nodos esclavos en el servidor de redis para no afectar el rendimiento.
 
En el siguiente link se puede ver la distribución de aplicaciones y cómo se relacionan con las apis externas y la memoria cache y el escalado del conjunto. [infraestructura](https://drive.google.com/file/d/1ZImlLzmVbBUs_I0zmzZSuqFVUFEtREI2/view?usp=sharing)
 
## Funcionamiento de la aplicación
El principal objetivo del siguiente [diagrama de flujo](https://drive.google.com/file/d/1qWXHS5R_pzoHh0_M3AJde_wrowQbIMA4/view?usp=sharing) es entender como el proxy administra la memoria cache de modo de evitar las coliciones. Para ellos se utilizan las listas que proporciona Redis que permite insertar para una misma key varios values.
 
## Uso
El main de la aplicación se encuentra en **cmd/proxy/** y se ejecuta con el comando `go run main.go`